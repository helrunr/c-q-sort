#include <stdio.h>
#include <stdlib.h>


int srt(const void *a, const void *b)
{
    int x = *(const int *)a;
    int y = *(const int *)b;

    if (x < y)
    {
        return -1;
    } else if (x > y) {
        return 1;
    }

    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
    int i_array[] = {12,23,54,76,123,74,876,234};
    float f_array[] = {3.4,55.3,1.12,5.23,1.11,87.34};
    int indx, i_total, f_total = 0;

    i_total = *(&i_array + 1) - i_array;
    f_total = *(&f_array + 1) - f_array;

    qsort(i_array, i_total, 4, srt);
    for (indx = 0; indx < i_total; indx++)
    {
        printf("%d ", i_array[indx]);
    }

    printf("\n");

    qsort(f_array, f_total, 4, srt);
    for (indx = 0; indx < f_total; indx++)
    {
        printf("%f ", f_array[indx]);
    }

    exit(EXIT_SUCCESS);
}
